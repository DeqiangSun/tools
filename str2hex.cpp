#include <stdio.h>

/*
let the data string formed to the real hex number
i.e: 12->0x12, ab->0xab
*/
namespace demo {
  void str2hex()
  {
    unsigned char szData[1024] = {0};
    char szIn[2014] = "12a11aff";
    int i = 0, j = 0;

    for (i = 0; i < 8; i+=2) {
      sscanf(szIn + i, "%2x", szData + j++);
    }

    for (i = 0; i < 4; ++i) {
      printf("%x ", szData[i]);
      printf("%d ", szData[i]);
    }
    return;
  }
}

int main(int argc, char* argv[])
{
	FILE* pFile = NULL;
	pFile = fopen("ascin.txt", "r");
	int i = 0, len = 0;
	unsigned char szData[1024] = {0};
	while (!feof(pFile)) {
		fscanf(pFile, "%2x", szData + i++);
	}
	fclose(pFile);
	len = i;
	pFile = NULL;
	pFile = fopen(argv[1], "wb");
	// fprintf(pFile, "%2x", szData + i++);	
	fwrite(szData, 1, len - 1, pFile);
	//	demo::str2hex();
	return 0;
}


